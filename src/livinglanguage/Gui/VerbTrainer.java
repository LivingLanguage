package livinglanguage.Gui;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class VerbTrainer extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JLabel jLabelFirstPersonSingular = null;
	private JLabel jLabelSecondPersonSingular = null;
	private JLabel jLabelThirdPersonSingular = null;
	private JLabel jLabelFirstPersonPlural = null;
	private JLabel jLabelSecondPersonPlural = null;
	private JLabel jLabelThirdPersonPlural = null;
	private JTextField jTextFieldFirstPersonSingular = null;
	private JTextField jTextFieldSecondPersonSingular = null;
	private JTextField jTextFieldThirdPersonSingular = null;
	private JTextArea jTextAreaFirstPersonPlural = null;
	private JTextField jTextFieldThirdPersonPlural = null;
	private JTextArea jTextAreaSecondPersonPlural = null;

	/**
	 * This is the default constructor
	 */
	public VerbTrainer() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(875, 433);
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabelThirdPersonPlural = new JLabel();
			jLabelThirdPersonPlural.setText("3rd person plural");
			jLabelSecondPersonPlural = new JLabel();
			jLabelSecondPersonPlural.setText("2nd person plural");
			jLabelFirstPersonPlural = new JLabel();
			jLabelFirstPersonPlural.setText("1st person plural");
			jLabelThirdPersonSingular = new JLabel();
			jLabelThirdPersonSingular.setText("3rd person singular");
			jLabelSecondPersonSingular = new JLabel();
			jLabelSecondPersonSingular.setText("2nd person singular");
			jLabelFirstPersonSingular = new JLabel();
			jLabelFirstPersonSingular.setText("1st person singular");
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(6);
			gridLayout.setVgap(7);
			gridLayout.setColumns(2);
			gridLayout.setHgap(10);
			jContentPane = new JPanel();
			jContentPane.setLayout(gridLayout);
			jContentPane.add(jLabelFirstPersonSingular, null);
			jContentPane.add(getJTextFieldFirstPersonSingular(), null);
			jContentPane.add(jLabelSecondPersonSingular, null);
			jContentPane.add(getJTextFieldSecondPersonSingular(), null);
			jContentPane.add(jLabelThirdPersonSingular, null);
			jContentPane.add(getJTextFieldThirdPersonSingular(), null);
			jContentPane.add(jLabelFirstPersonPlural, null);
			jContentPane.add(getJTextAreaFirstPersonPlural(), null);
			jContentPane.add(jLabelSecondPersonPlural, null);
			jContentPane.add(getJTextAreaSecondPersonPlural(), null);
			jContentPane.add(jLabelThirdPersonPlural, null);
			jContentPane.add(getJTextFieldThirdPersonPlural(), null);
			
		}
		return jContentPane;
	}

	/**
	 * This method initializes jTextFieldFirstPersonSingular	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldFirstPersonSingular() {
		if (jTextFieldFirstPersonSingular == null) {
			jTextFieldFirstPersonSingular = new JTextField();
		}
		return jTextFieldFirstPersonSingular;
	}

	/**
	 * This method initializes jTextFieldSecondPersonSingular	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldSecondPersonSingular() {
		if (jTextFieldSecondPersonSingular == null) {
			jTextFieldSecondPersonSingular = new JTextField();
		}
		return jTextFieldSecondPersonSingular;
	}

	/**
	 * This method initializes jTextFieldThirdPersonSingular	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldThirdPersonSingular() {
		if (jTextFieldThirdPersonSingular == null) {
			jTextFieldThirdPersonSingular = new JTextField();
		}
		return jTextFieldThirdPersonSingular;
	}

	/**
	 * This method initializes jTextAreaFirstPersonPlural	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getJTextAreaFirstPersonPlural() {
		if (jTextAreaFirstPersonPlural == null) {
			jTextAreaFirstPersonPlural = new JTextArea();
		}
		return jTextAreaFirstPersonPlural;
	}

	/**
	 * This method initializes jTextFieldThirdPersonPlural	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldThirdPersonPlural() {
		if (jTextFieldThirdPersonPlural == null) {
			jTextFieldThirdPersonPlural = new JTextField();
		}
		return jTextFieldThirdPersonPlural;
	}

	/**
	 * This method initializes jTextAreaSecondPersonPlural	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextArea getJTextAreaSecondPersonPlural() {
		if (jTextAreaSecondPersonPlural == null) {
			jTextAreaSecondPersonPlural = new JTextArea();
		}
		return jTextAreaSecondPersonPlural;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
