package livinglanguage.Gui;

import java.awt.AWTKeyStroke;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class VerbConjugation extends JPanel {

	private static final long serialVersionUID = 1L;
	private JPanel jPanelVerbHeader = null;
	private JPanel jVerbConjucation = null;
	private JLabel jLabelFirstPersonSingular = null;
	private JTextField jTextFieldFirstPersonSingular = null;
	private JLabel jLabelSecondPersonSingular = null;
	private JTextField jTextFieldSecondPersonSingular = null;
	private JLabel jLabelThirdPersonSingular = null;
	private JTextField jTextFieldThirdPersonSingular = null;
	private JLabel jLabelFirstPersonPlural = null;
	private JTextField jTextFieldFirstPersonPlural = null;
	private JLabel jLabelSecondPersonPlural = null;
	private JTextField jTextFieldSecondPersonPlural = null;
	private JLabel jLabelThirdPersonPlural = null;
	private JTextField jTextFieldThirdPersonPlural = null;
	private JLabel jLabelVerbName = null;  //  @jve:decl-index=0:visual-constraint="626,140"
	private JTextField jTextFieldVerbName = null;  //  @jve:decl-index=0:visual-constraint="675,147"
	/**
	 * This is the default constructor
	 */
	public VerbConjugation() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(380, 253);
		this.setLayout(new BorderLayout());
		this.add(getJPanelVerbHeader(), BorderLayout.NORTH);
		this.add(getJVerbConjucation(), BorderLayout.CENTER);
		
		setTabFocus(getJPanelVerbHeader());
		setTabFocus(getJVerbConjucation());
		setTabFocus(this);
	}


	private void setTabFocus(JPanel component)
	{
		Set<AWTKeyStroke> keystrokes = component.getFocusTraversalKeys(
				KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
		keystrokes = new HashSet<AWTKeyStroke>(keystrokes);
		KeyStroke forward = KeyStroke.getKeyStroke("TAB");
		keystrokes.add(forward);
		component.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, keystrokes);
	}
	        	/**
	 * This method initializes jPanelVerbHeader	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJPanelVerbHeader() {
		if (jPanelVerbHeader == null) {
			jPanelVerbHeader = new JPanel();
			jPanelVerbHeader.setLayout(new BoxLayout(jPanelVerbHeader, BoxLayout.X_AXIS));
			//jPanelVerbHeader.add(jLabelVerbName, null);
			jPanelVerbHeader.add(getJLabelVerbName(), null);
			jPanelVerbHeader.add(getJTextFieldVerbName(), null);
			
		}
		return jPanelVerbHeader;
	}

	/**
	 * This method initializes jVerbConjucation	
	 * 	
	 * @return javax.swing.JPanel	
	 */
	private JPanel getJVerbConjucation() {
		if (jVerbConjucation == null) {
			jLabelThirdPersonPlural = new JLabel();
			jLabelThirdPersonPlural.setText("3rd person plural");
			jLabelSecondPersonPlural = new JLabel();
			jLabelSecondPersonPlural.setText("2nd person plural");
			jLabelFirstPersonPlural = new JLabel();
			jLabelFirstPersonPlural.setText("1st person plural");
			jLabelThirdPersonSingular = new JLabel();
			jLabelThirdPersonSingular.setText("3rd person singular");
			jLabelSecondPersonSingular = new JLabel();
			jLabelSecondPersonSingular.setText("2nd person singular");
			jLabelFirstPersonSingular = new JLabel();
			jLabelFirstPersonSingular.setText("1st person singular");
			GridLayout gridLayout = new GridLayout();
			gridLayout.setRows(6);
			gridLayout.setHgap(10);
			gridLayout.setVgap(7);
			gridLayout.setColumns(2);
			jVerbConjucation = new JPanel();
			jVerbConjucation.setLayout(gridLayout);
			jVerbConjucation.add(jLabelFirstPersonSingular, null);
			jVerbConjucation.add(getJTextFieldFirstPersonSingular(), null);
			jVerbConjucation.add(jLabelSecondPersonSingular, null);
			jVerbConjucation.add(getJTextFieldSecondPersonSingular(), null);
			jVerbConjucation.add(jLabelThirdPersonSingular, null);
			jVerbConjucation.add(getJTextFieldThirdPersonSingular(), null);
			jVerbConjucation.add(jLabelFirstPersonPlural, null);
			jVerbConjucation.add(getJTextAreaFirstPersonPlural(), null);
			jVerbConjucation.add(jLabelSecondPersonPlural, null);
			jVerbConjucation.add(getJTextAreaSecondPersonPlural(), null);
			jVerbConjucation.add(jLabelThirdPersonPlural, null);
			jVerbConjucation.add(getJTextFieldThirdPersonPlural(), null);
		}
		return jVerbConjucation;
	}

	/**
	 * This method initializes jTextFieldFirstPersonSingular	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldFirstPersonSingular() {
		if (jTextFieldFirstPersonSingular == null) {
			jTextFieldFirstPersonSingular = new JTextField();
		}
		return jTextFieldFirstPersonSingular;
	}

	/**
	 * This method initializes jTextFieldSecondPersonSingular	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldSecondPersonSingular() {
		if (jTextFieldSecondPersonSingular == null) {
			jTextFieldSecondPersonSingular = new JTextField();
		}
		return jTextFieldSecondPersonSingular;
	}

	/**
	 * This method initializes jTextFieldThirdPersonSingular	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldThirdPersonSingular() {
		if (jTextFieldThirdPersonSingular == null) {
			jTextFieldThirdPersonSingular = new JTextField();
		}
		return jTextFieldThirdPersonSingular;
	}

	/**
	 * This method initializes jTextAreaFirstPersonPlural	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextField getJTextAreaFirstPersonPlural() {
		if (jTextFieldFirstPersonPlural == null) {
			jTextFieldFirstPersonPlural = new JTextField();
		}
		return jTextFieldFirstPersonPlural;
	}

	/**
	 * This method initializes jTextAreaSecondPersonPlural	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextField getJTextAreaSecondPersonPlural() {
		if (jTextFieldSecondPersonPlural == null) {
			jTextFieldSecondPersonPlural = new JTextField();
		}
		return jTextFieldSecondPersonPlural;
	}

	/**
	 * This method initializes jTextFieldThirdPersonPlural	
	 * 	
	 * @return javax.swing.JTextField	
	 */
	private JTextField getJTextFieldThirdPersonPlural() {
		if (jTextFieldThirdPersonPlural == null) {
			jTextFieldThirdPersonPlural = new JTextField();
		}
		return jTextFieldThirdPersonPlural;
	}

	/**
	 * This method initializes jLabelVerbName	
	 * 	
	 * @return javax.swing.JLabel	
	 */
	private JLabel getJLabelVerbName() {
		if (jLabelVerbName == null) {
			jLabelVerbName = new JLabel();
			jLabelVerbName.setText("Verb: ");
		}
		return jLabelVerbName;
	}

	/**
	 * This method initializes jTextAreaVerbName	
	 * 	
	 * @return javax.swing.JTextArea	
	 */
	private JTextField getJTextFieldVerbName() {
		if (jTextFieldVerbName == null) {
			jTextFieldVerbName = new JTextField();
			
		}
		return jTextFieldVerbName;
	}

}  //  @jve:decl-index=0:visual-constraint="10,10"
