package livinglanguage.model;

public abstract class Word {


	private String name;

	public String getName() {
		return name;
	}

	protected Word()
	{
		name = "";
	}

	public  Word(String word)
	{
		this.name = word;
	}


}
