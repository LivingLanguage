package livinglanguage.model.german;

public class Verb extends livinglanguage.model.Verb {


	public enum Time {
		Praesens, Perfect, Praeteritum, Plusquamperfect, FuturI, FuturII
	}

	public enum Modus {
		Indicativ, KonjuktivI, KonjuktivII, Konditional, Imperativ
	}

	public enum Person {
		FirstSingular, SecondSingular, ThirdSingular, FirstPlural, SecondPlural, ThirdPlural
	}

	private final Modus modus;
	private final Time time;
	private final Person person;

	
	public Verb(Modus modus, Time time, Person person, String verb)
	{
		super(verb);
		this.time = time;
		this.person = person;
		this.modus = modus;
	}
	
	public Modus getModus() {
		return this.modus;
	}

	public Person getPerson() {
		return this.person;
	}

	public Time getTime() {
		return this.time;
	}
}
